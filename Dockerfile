FROM python:3.9-slim

COPY requirements.txt /app/django/requirements.txt

RUN pip install -r /app/django/requirements.txt

COPY api /app/django/api
COPY config /app/django/config
COPY manage.py /app/django/manage.py

WORKDIR /app/django
