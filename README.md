# Autenticação Oauth simples para NodeRed através Bitbucket 

Aplicação para executar o NodeRed usando o Django como uma camada de autenticação.
Possui suporte para autenticar via login e senha.

# Pré-requisitos
- Python  3.7+
- Docker
- Docker-Compose

# Iniciando

Execute a aplicação:
```
docker-compose up -d
```

Executa as migrações:
```
docker-compose exec auth python manage.py migrate
```

Colete os arquivos staticos:
```
docker-compose exec auth python manage.py collectstatic --no-input
```

# Configuração

Crie um superusuário:
```
docker-compose exec auth python manage.py createsuperuser
```

Acessar a administração e cadastrar a key e o secret do bitbucket:
```
/nodered/auth/admin/
```
