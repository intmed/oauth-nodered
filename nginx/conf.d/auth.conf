proxy_cache_path                /tmp/auth_cache levels=1:2 keys_zone=auth_cache:1m max_size=1g inactive=60m;

server {
    listen      80;
    server_name _;

    charset     utf-8;

    client_max_body_size 15M;

    location /nodered/ {
        rewrite /nodered/(.*) /$1 break;
        auth_request     /auth;
        auth_request_set $backend_status $upstream_status;
        error_page 401 @auth;
        proxy_redirect     off;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header IP $remote_addr;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "upgrade";
        proxy_pass http://nodered:1880;
    }

    location /nodered/auth/static/ {
        alias /app/static/;
    }

    location /nodered/auth/ {
        rewrite /nodered/(.*) /$1 break;
        proxy_redirect     off;
        proxy_http_version 1.1;
        proxy_set_header Host $host;
        proxy_set_header IP $remote_addr;
        proxy_pass http://auth:8000;
    }

    location /auth {
        internal;
        proxy_set_header        Content-Length "";
        proxy_set_header        Host $host;
        proxy_set_header        IP $remote_addr;
        proxy_set_header        X-Original-URI $request_uri;
        proxy_pass              http://auth:8000/auth/api/is-authenticated/;
        proxy_cache             auth_cache;         # Enable caching
        proxy_cache_key         $cookie_user_id;    # Cache for each user session
        proxy_cache_lock        on;                 # Duplicate tokens must wait
        proxy_cache_valid       200 30s;            # How long to use each response
        proxy_ignore_headers    Cache-Control Expires Set-Cookie;
    }

    location @auth {
        if ($backend_status = "401") {
           return 302 /nodered/auth/accounts/login/;
        }
    }
}
