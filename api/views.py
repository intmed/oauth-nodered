from django.http import JsonResponse


def is_authenticated_view(request):
    status = 401
    is_authenticated = request.user.is_authenticated
    if is_authenticated:
        status = 200
    return JsonResponse({'authenticated': is_authenticated}, status=status)
