from django.urls import path
from api.views import is_authenticated_view


urlpatterns = [
    path('is-authenticated/', is_authenticated_view, name='is-authenticated')
]
